from maven:3.3-alpine

RUN apk add --update --no-cache git openssh-client && \
    git clone https://bitbucket.org/leochung/testframeworkforimage/overview && \
    cd overview && \
    mvn clean test ; exit 0 && \
    cd .. && \
    rm -rf overview